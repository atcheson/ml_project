This repository contains implementations of rating prediction systems built for the COMP9417 project
by Mia Atcheson, Sarith Fernando and Sandeepa Kannangara. Each system is documented independently;
see the readme files in each subdirectory for details, and refer to report/report.pdf for full
descriptions and evaluations of the algorithms. 

The following subdirectories are included:

- gplsa: implementation of the Gaussian probabilistic latent semantic analysis algorithm
- memory_based: implementations of the k-nearest-neighbour and signular value decomposition algorithms
- tag_recommender: implementation of the tag-based system
- fusion: implementation of the artificial neural network to fuse subsystem outputs
- report: LaTeX source and typeset pdf for the project report