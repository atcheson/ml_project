\documentclass{article}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{titlesec}
\usepackage{bm}
\usepackage{multirow}
\usepackage[nottoc,numbib]{tocbibind}
\usepackage[toc,page]{appendix}

\graphicspath{ {figures/} }

\begin{document}
\title{COMP9417 Project Report}
\author{Mia Atcheson \\ (5090831) \and Sarith Fernando \\ (5079313) \and Sandeepa Kannangara \\ (5033239)}
\maketitle

\tableofcontents
\pagebreak
\section{Introduction}
In this project, we address Topic 9: \textit{Recommender system using collaborative filtering}. Specifically, we have built a system to carry out the \textit{predict task} on the MovieLens data sets: we wish to predict the star rating that would be given by a particular user to a particular movie, given the available data regarding previous user ratings of movies and applications of descriptive tags to movies.
\par
In order to address this task, we have constructed several separate prediction systems: a system based on nearest-neighbour analysis of users, a system utilizing decomposition of the ratings matrix for dimensionality reduction; a system based on probabilistic modelling of users' rating behavior, and a system that utlilizes user applications of descriptive tags to movies to infer users' preferences. Finally, we have built a fusion system to take the outputs of each of these systems and process them to generate a combined prediction.

\section{Description of Algorithms}
\subsection{Memory-based Collaborative Filtering}
\subsubsection{User-User Collaborative Filtering}\label{sec:knn}
One of the most popular collaborative filtering methods is user-user collaborative filtering \cite{RN155}. This method is also known as $k$-nearest-neighbour collaborative filtering, because it predicts a user's rating for an item based on the ratings of the $k$ most similar users, based on a user similarity measure calculated from users' rating vectors. A primary advantage of this method is its simplicity of implementation and interpretation, while still producing competitive results. However, it may require a large amount of memory when the number of users is large. Given a user $u$ and an item $i$ for which we wish to predict a rating, the algorithm can be summarized as follows:
\begin{enumerate}
\item Determine the set $U_i$ of users who have rated item $i$
\item Calculate the similarity $s(u, u')$ between each user $u'$ in $U_i$ and the user of interest $u$
\item Compute the predicted rating by taking the average rating for item $i$ by users in $U_i$, weighted by their similarity to the user of interest $s(u, u')$
\end{enumerate}
As noted above, in order to compute predictions, we must define a similarity function to calculate the similarity between pairs of users. This similarity function is a critical design decision which will directly affect the performance of the predictor. Several similarity functions have been proposed in the literature, including cosine similarity, Pearson correlation, and others \cite{RN157}. However in collaborative filtering applications such as this one, because of the sparsity of the user-item ratings matrix, Pearson correlation is generally held to perform better than cosine similarity \cite{RN158,RN159}. The Pearson correlation computes a statistical correlation between two users by considering only items which both users have rated. This can be computed as:
$$
s(u, v) = \frac{\sum\limits_{i \in I_u \cap I_v}(r_{u, i} - r'_u)(r_{v, i} - r'_v)}{\sqrt{\sum\limits_{i \in I_u \cap I_v}(r_{u, i} - r'_u)^2}\sqrt{\sum\limits_{i \in I_u \cap I_v}(r_{v, i} - r'_v)^2}}
$$
where $r_{u, i}$ is the rating of user $u$ for item $i$. In addition,  $r'_u$ is the average rating for user $u$. A drawback of this method is that it may calculate a high similarity between users even with very few ratings. However, this can be overcome by setting a threshold for the minimum number of common ratings between two users \cite{RN159,RN160}. 
\par
Once user similarities have been computed, to make a prediction of a user's rating for a given item, the $k$ most similar users are selected based on the value of the similarity function.
Then, to generate predictions, we combine the ratings of these $k$ users together, typically with an average weighted by similarity score. Before computing the weighted average, some normalization can be performed to reduce the effects of users with unusually high or low average ratings or unusually high or low variance in their rating behavior. This is typically done by subtracting the mean rating for each user and normalizing the standard deviation of ratings for each user. With normalized per-user mean and standard deviation, the predicted rating for item $i$ by user $u$ can be calculated as:
$$
p_{u, i} = r'_u + \sigma_u\frac{\sum\limits_{u' \in k}{s(u, u')(r_{u',i} - r'_{u'})/\sigma_{u'}}}{\sum\limits_{u' \in k}|s(u, u')|}
$$
where $\sigma_u$ is the standard variation of ratings by user $u$. Although there are several ways to calculate weighted average over the $k$ most similar users, this is the most commonly used method for computing predictions in this approach \cite{RN156,RN161}.

\subsubsection{Dimensionality reduction}\label{sec:svd}
In a typical dataset to which CF methods are applied, there are large numbers of users and items, with only a small subset of user-item pairs having an associated rating, resulting in a sparse user-item rating matrix of high dimensionality. This suggests an approach which attempts to reduce the dimensionality of the user-item rating matrix in order to make predictions. In particular, singular value decomposition (SVD) has been used successfully in CF problems \cite{RN162,RN164}. In SVD the user-item rating matrix $\bm{M}$ is factorized into three constituent matrices which are $\bm{U}$, $\bm{\Sigma}$ and $\bm{T}$ as follows:
$$
\bm{M} = \bm{U}\bm{\Sigma} \bm{T}^T
$$
where $\bm{\Sigma}$  is the diagonal matrix of singular values and $\bm{U}$ and $\bm{T}$ are orthogonal matrices. The matrix $\bm{U}$ represents user-topic interest values and $\bm{T}$ represents item-topic interest values. However, SVD can only be applied to complete matrices, whereas there are typically many user-item pairs for which no rating exists. Therefore, prior to calculating the SVD of the user-item ratings matrix, it is necessary to somehow fill in the missing values. In general, using the per-item average value seems to be a successful approach for the missing-value imputation step, and this approach seems to outperform the per-user average \cite{RN164}. Once the SVD has been computed, the predicted user rating $p_{u, i}$ for a given item can be calculated very efficiently by taking the weighted dot product of the item-topic interest vector $\bm{i}$ and the user-topic interest vector $\bm{u}$:
\begin{equation}
\label{eq:predict_dm}
p_{u, i} = \sum\limits_f u_f \sigma_f i_f
\end{equation}
where $\sigma_f$ is the element of $\bm{\Sigma}$ corresponding to topic $f$, and $u_f$ and $i_f$ are the elements of $\bm{u}$ and $\bm{i}$, respectively. For a given user $u$, the complete vector $p_u$ of predicted ratings for all the items can be directly calculated by multiplying the user-topic interest vector with the scaled item matrix $\bm{\Sigma} \bm{T}^T$:
$$
p_u = \bm{u}\bm{\Sigma} \bm{T}^T
$$
This algorithm considers the ratings of all users in its prediction of ratings, but we may only wish to consider the ratings of users which are similar to the user for which we are attempting to make a prediction. In this case, we can calculate user-user similarity scores by taking the dot products of user-topic interest vectors \cite{RN162,RN163,RN164}. Then, ratings can be predicted using a $k$-nearest-neighbour process similar to that descibed in Section \ref{sec:knn}.
\subsection{Probabilistic Modelling}\label{sec:gmm}
\subsubsection{Introduction}
Probabilistic methods for collaborative filtering (CF) aim to build probabilistic models of user behavior by implementing systems to learn and recognize complex patterns based on training data. These models are then used to predict future behavior \cite{Schafer2007}. Model based CF algorithms including Bayesian models, clustering models and dependency networks have been investigated to solve the shortcomings of memory-based CF algorithms \cite{Hofmann2003}. The primary goal of probabilistic models for the predict task is to estimate P$\left(r_{u,i}|u\right)$, the probability distribution over user \textit{u}'s rating of item \textit{i}.

Latent semantic analysis is a widely-used technique which relies on statistical modeling using latent class variables and mixture models to predict user ratings for particular items \cite{Su2009}. Compared to memory-based methods, these approaches have been shown to achieve higher accuracy and scalability in many scenarios \cite{Hofmann2001}. Probabilistic latent semantic analysis (pLSA) is a latent semantic CF approach proposed by Hofmann \cite{Hofmann2003, Hofmann2001}, which has achieved competitive recommendation and predictive accuracies on several datasets. Therefore, for this project we have implemented pLSA as a probabilistic modelling method for the rating prediction task.

\subsubsection{Gaussian Probabilistic Latent Semantic Analysis}
pLSA was originally developed in the context of information retrieval and referred to as probabilistic latent semantic indexing (pLSI) \cite{Schafer2007}. It is a matrix factorization technique which is similar to singular value decomposition (SVD), however it is based on statistical probability rather than linear algebra \cite{Hofmann2004}. In our implementation, we use  a variant of pLSA termed Gaussian pLSA \cite{ Hofmann2003}.

The primary assumption of this model is that the observed user ratings can be modeled as a mixture of user communities, where each user may participate probabilistically in one or more communities.  Each community is characterized by a Gaussian distribution on the normalized rating.

In the case of collaborative filtering, the data consists of a set of user-item-rating tuples $\left(u,i, r_{u, i}\right)$. The pLSA model uses a hidden variable $Z$ with states $z$ to represent the user communities or interest groups. The set of possible values of $Z$ is assumed to be finite and of size $k$. Therefore, the distribution can be decomposed as,

\begin{equation}\label{eqn:prob_model}
P\left(r_{ui} |u,i\right)  = \sum_{z=1}^{k} P\left(r_{ui} |i,z\right)P\left(z|u\right) 
\end{equation}

where the mixing proportions $P(z|u)$ represent the strength of a user's membership in each community \cite{Liu2009}. 

Before calculating the model parameters we first normalize the user-item ratings by adopting the following normalization model,

$$
P\left(r_{ui}|u,i\right) = \mu_{u} + \sigma_{u} \sum_{z} P\left(z|u\right)p(\dfrac{r_{ui}-\mu_{u}}{\sigma_{u}};\mu_{i,z},\sigma_{i,z})
$$

where $\mu_{u}$ denotes the user-specific mean rating and $\sigma_{u}$ denotes the user-specific standard variation for user $u$, and $\mu_{i,z}$ and $\sigma_{i,z}$ denote the mean and standard deviation of ratings applied to item $i$ by users, weighted by membership in community $z$. To allow the algorithm to gracefully handle users and items with only a small number of ratings, we use the following equations to smooth the estimated means and variances, \cite{Hofmann2003}

$$
\mu_{u} = \dfrac{\sum\limits_{r_{ui}} r_{ui}+ q \bar{\mu}}{N_{u} + q}
$$
$$
\sigma_{u} =  \dfrac{\sum\limits_{r_{ui}} \left( r_{ui} - \mu_{u} \right)^{2} + q\bar{\sigma}^{2}}{N_{u} + q}
$$

where $\bar{\mu}$ denotes the mean rating and $\bar{\sigma}^{2}$ denotes the overall variance of ratings. $N_{u}$ is the number of ratings available for a user and $q$ is a free parameter controlling the smoothing strength, which in our implementation was set to $5$.

When the rating values are numeric, a Gaussian distribution can be used to model the rating value for each item $i$ in each latent class $z$ by introducing a mean parameter and a variance parameter \cite{Liu2009}. Therefore, the model defined in Equation \ref{eqn:prob_model}  can viewed as a Gaussian mixture model \cite{Hofmann2003}:

\begin{equation}\label{eqn:gaussian_mixture}
p\left(r_{ui}|u,i\right) = \sum_{z=1}^{k} P\left(z|u\right)P\left(r_{ui};\mu_{zi},\sigma_{zi}\right)  
\end{equation}

$$
P\left(r_{ui};\mu_{zi},\sigma_{zi}\right) = \dfrac{1}{\sqrt{z\pi}\sigma_{zi}}exp\left[-\dfrac{\left(r_{ui}-\mu_{zi} \right)^{2} }{\sigma_{zi}^{2}} \right] 
$$

Then to make predictions, we compute the expected rating that would be assigned to item $i$ by user $u$:

$$
\hat{r}_{ui} = \sum_{z} P\left(z|u\right)\mu_{zi} 
$$

The model defined in Equation \ref{eqn:gaussian_mixture} needs to calculate $n \times  k$ means $\mu_{zi}$, $n \times k$ variances $\sigma_{zi}^{2}$ and $m \times k$ user-dependent mixing proportions $P\left(z|u\right) $ where $n$ is the number of items and $m$ is the number of users. Therefore, as proposed by \cite{Hofmann2003}, we have adopted the expectation maximization (EM) algorithm to estimate these parameters by maximizing the log-likelihood of all the observed ratings.

In the expectation step of the EM algorithm, the posterior probabilities of the latent variables are computed:

$$
P\left(z|u,i\right) = \dfrac{P\left(z|u\right) P\left(r_{ui};\mu_{zi},\sigma_{zi}\right) }{\sum_{z^{'}=1}^{k}P\left(z^{'}|u\right)P\left(r_{ui};\mu_{z^{'}i},\sigma_{z^{'}i}\right)  }
$$

In the maximization step, the parameters $\mu_{zi}$, $\sigma_{zi}^{2}$ and $P\left(z|u\right) $ are obtained by maximizing the expected complete log-likelihood $E\left[\mathcal{L}^{c}\right]$:

$$
E\left[\mathcal{L}^{c}\right] =\sum_{\left(u,i\right)\in R } \sum_{z=1}^{k} P\left(z|u,i\right) log\left[P\left(r_{ui};\mu_{zi},\sigma_{zi}\right) P\left(z|u\right)  \right]  
$$

Therefore, in the maximization step $\mu_{zi}$, $\sigma_{zi}^{2}$ and $P\left(z|u\right) $ are calculated as follows:

$$
P\left(z|u\right) = \dfrac{\sum _{\left(u^{'} ,i\right) \in R: u^{'}=u } P\left(z|u,i\right) }{\sum_{z^{'}=1}^{k} \sum_{\left(u^{'} ,i\right) \in R: u^{'}=u} P\left(z^{'}|u,i\right) }
$$

$$
\mu_{zi} = \dfrac{\sum_{\left(u,i^{'}\right) \in R: i^{'}=i} r_{ui} P\left(z|u,i\right) }{\sum_{\left(u,i^{'}\right) \in R: i^{'}=i} P\left(z|u,i\right) }
$$

$$
\sigma_{zi}^{2} = \dfrac{\sum_{\left(u,i^{'}\right) \in R: i^{'}=i} \left( r_{ui} - \mu_{zi}\right)^{2}  P\left(z|u,i\right)}{\sum_{\left(u,i^{'}\right) \in R: i^{'}=i} P\left(z|u,i\right)}
$$

The expectation step and the maximization step are alternately repeated until the stopping condition is met, to avoid overfitting. The criterion we have used is to stop when the absolute difference between the root mean squared error (RSME) on the training data of the current iteration and the previous iteration is less than $0.0001$ of the RMSE of the previous iteration. Once this condition is met, the EM algorithm is terminated, and we generate the matrix of expected ratings for all the movies by all the users.

To determine the number of latent classes, the model was tested on the MovieLens 100k dataset with the number of classes varying from 5 to 60. We found that the performance of the system improved with the number of latent classes, as illustrated in Figure \ref{fig:gplsa_classes} in Appendix \ref{app:fig}. However, due to computational resource constraints, we were only able to use 20 classes with the full evaluation dataset. 
\subsection{Tag-based Inference} \label{sec:tag}
\subsubsection{Introduction}
The tag-based system uses information represented by users' applications of descriptive tags to movies in order to generate predictions of user ratings. Intuitively, we expect that these tags will carry information about various aspects of movies to which they are applied, and that users will have differing preferences towards different aspects of movies, which can be used to inform our prediction of how they will rate movies with particular aspects. For example, a particular user may have a preference for animated movies, and animated movies should be more likely to have the tag \textit{animated} applied to them more times, so we can expect that this user will tend to rate movies with that tag more highly than average. 
\par
The implementation we have built is based primarily on the work presented in \cite{sen2009tagommenders}, and the initial measure of movie-tag relevance we have used is motivated by the work presented in \cite{sen2009learning}.
\par
 Although tag-based systems are generally held to perform less accurately on the predict task than traditional CF methods which operate directly on the ratings matrix, we have implemented this system in the expectation that it may provide an additional source of information to the complete hybrid system which is not highly correlated with the results of the other systems, as it is able to utilize information represented by the tag applications, which is not incorporated into the other systems. Broadly, the tag-based system we implemented works as follows:
\begin{enumerate}
\item Movies, tags, and users are pruned to produce a subset with a sufficient density of ratings and tag applications to perform meaningful inference.
\item We estimate the \textit{relevance} of a tag to a movie by the number of times that tag has been applied to that movie as a proportion of the total number of tag applications to that movie.
\item We use the relevance information to calculate an estimate of each user's \textit{preference} for each tag, based upon how highly they have rated movies weighted by the movie's relevance to each tag.
\item We use the user-tag preference information to calculate the \textit{similarity} between each movie and each tag based on the cosine similarity between each user's ratings for that movie and their inferred preferences for that tag. This measure differs from the relevance estimate, as it provides a similarity score between every movie-tag pair, not just for those tags which have actually be applied to a given movie.
\item We use the similarity and preference information to predict a user's rating for a movie by taing the average of the user's preferences for the most similar tags to that movie, weighted by their similarity.
\end{enumerate}
Figure \ref{fig:tag_diagram} provides a diagrammatic overview of the process described above.
\begin{figure}[h]
\makebox[\textwidth][c]{\includegraphics[width=1.1\textwidth]{tag_diagram.png}}
\caption{Diagrammatic representation of the flow of information in the tag-based system}
\label{fig:tag_diagram}
\end{figure}
\subsubsection{Detailed Description}
\paragraph{Pruning step}
In order to reduce the computation required to carry out the tag-based algorithm, the full set of users, movies and tags are initially pruned to produce a smaller subset for which we expect that we may be able to make meaningful inferences. They are pruned as follows:
\begin{itemize}
\item Users who have rated fewer than five movies are removed, as we do not expect to be able to infer a meaningful estimate of their tag preferences with so few ratings.
\item Movies and tags are iteratively pruned to produce a stable set in which each movie has at least five tags applied to it, and each tag has been applied to at least five movies, as we do not expect to be able to meaningfully generalize about tags which have been applied to only very few movies, or generate useful tag-based predictions for movies with very few tags.
\end{itemize}
When used as part of the complete hybrid system, we simply predict the user's mean rating for user-movie pairs which do not appear in the pruned subset.
\paragraph{Movie-tag relevance}
We make our initial estimate of a the relevance $w(m, t)$ of a tag $t$ to a movie $m$ by simply taking the number of times that tag has been applied to that movie as a proportion of the total number of tag applications to that movie. This is motivated by \cite{sen2009learning}, which presented evidence that this method provides a reasonable measure of tag relevance given the data available to us for this task.
\paragraph{User-tag preference}
To infer users' preferences for tags, we implemented the \texttt{movie-ratings} algorithm from \cite{sen2009tagommenders}, due to its favorable trade-off between  tractability and accuracy. Given a user $u$, a tag $t$, the set of movies $M_t$ to which that tag has been applied, and user-movie ratings $r_{u, m}$, we infer the user-tag preference as:
$$
\text{tp}(u, t) = \frac{\sum_{m \in M_t}w(m, t)r_{u, m}}{\sum_{m \in M_t}w(m, t)}
$$
To reduce the effect of inter-tag and inter-user variability, the matrix of user-tag preferences is then processed by first normalizing the per-tag means to 0 and standard deviations to 1, and then normalizing the per-user means to 0 to produce normalized user-tag preferences $\text{ntp}(u, t)$.
\paragraph{Movie-tag similarity}
To infer similarity scores between each movie-tag pair, we use the \texttt{cosine-tag algorithm} described in \cite{sen2009tagommenders}. This method uses the cosine similarity between ratings for a movie and inferred tag preferences to infer the similarity between a movie and a tag. Given a movie $m$, a tag $t$, the set of users $U_m$ who have rated the movie, user-movie ratings $r_{u, m}$, average user ratings $\bar{r_u}$ and normalized inferred tag preferences $\text{ntp}(t, u)$, the movie-tag similarity is calculated as:
$$
\text{sim}(m, t) = \frac{\sum\limits_{u \in U_m}(r_{u, m} - \bar{r_u})\text{ntp}(u, t)}{\left(\sum\limits_{u \in U_m}(r_{u, m} - \bar{r_u})\right)^\frac{1}{2}\left(\sum\limits_{u \in U_m}\text{ntp}(u, t)\right)^\frac{1}{2}}
$$
\paragraph{Predicted user rating}
Once we have calculated the normalized inferred user-tag preferences $\text{ntp}(u, t)$ and the inferred movie-tag similarities $\text{sim}(m, t)$, and given a user $u$, a movie $m$ and the user's average rating $\bar{r_{u}}$ and the 30 tags $T_m$ applied to that movie with the highest similarity score (or fewer if there are fewer than 30 tags applied to movie $m$; this constant was determined empirically), we calculate that user's predicted rating for that movie as the user's average rating added to the average preference of that user for the tags in $T_m$, weighted by each tag's similarity score to the movie $m$:
$$
\text{predicted-rating}(u, m) = \frac{\sum\limits_{t \in T_m} \text{sim}(m, t)\text{ntp}(u, t)}{\sum\limits_{t \in T_m} \text{sim}(m, t)} + \bar{r_{u}}
$$
\subsection{Neural Network-based Fusion}\label{sec:ann}
In order to fuse the results of the subsystems descibed above into a hybrid system, we trained a two-layer feed-forward neural network. The network consists of an input layer taking the predicted ratings from each of the subsystems, a hidden layer with five nodes, and a single output node. The nodes in the hidden layer use a sigmoidal activation function:
$$
\phi(v) = \frac{2}{(1 + \exp(-2v))} -1 \text{, where } v = \sum\limits_i x_i w_i
$$
where $x_i$ and $w_i$ represent elements of the input and weight vectors, respectively. The output node uses a linear activation function, and a bias weight was applied to the hidden and output nodes in addition to the input weights. A diagram of the neural network configured to fuse the outputs of three subsystems is presented in Figure \ref{fig:ann} in Appendix \ref{app:fig}. The parameters for each fusion network were trained on held-out development data using Levenberg-Marquardt backpropagation \cite{hagan1994training} using the mean squared error as the error signal.

\section{Results}
\subsection{Methods of Evaluation}
To evaluate the performance of the implemented systems, we used data from the MovieLens 10M dataset \cite{harper2015movielens}. The first 1 milllion ratings from the dataset were partitioned randomly into training (50\%), development (30\%), and test (20\%) sets (we did not use all 10 million ratings in our evaluation due to computational resource constraints). 
\par
The training data (and additionally, in the case of the tag system, the tag application data supplied with the dataset) were used to train the individual systems, while the development data were used to train the neural network in order to fuse the ouputs of the individual subsystems to produce hybrid systems. Each system (hybrid and standalone) was then used to predict the test ratings.
\par
To evaluate and compare the performance of each system, we used two primary measures of rating prediction accuracy: 
\begin{itemize}
\item Mean absolute error (MAE): the average absolute difference between the predicted rating and the correct rating for each user-movie pair in the test set
\item Root mean squared error (RMSE): the square root of the average squared difference between the predicted rating and the correct rating for each user-movie pair in the test set
\end{itemize}
These measures were chosen due to their straightforward interpretation and very common use in the evaluation of collaborative filtering systems as applied to the predict task.
\subsection{Results}
The following systems were evaluated:
\begin{itemize}
\item \texttt{item-avg}: for a user-item pair $(u, i)$, uses the average rating from the training set for that item. Included as a na\"{\i}ve baseline with which to compare more complex systems.
\item \texttt{user-avg}: for a user-item pair $(u, i)$, uses the average rating from the training set from that user. Included as a na\"{\i}ve baseline with which to compare more complex systems.
\item \texttt{knn}: uses the $k$-nearest-neighbours algorithm descibed in Section \ref{sec:knn}, with $k = 60$
\item \texttt{svd}: uses the singular value decomposition-based algorithm descibed in Section \ref{sec:svd}, in particular the \texttt{SVD-user-similarity-weighted-rating} variant descibed in Appendix \ref{app:svd}.
\item \texttt{gplsa}: uses the Gaussian probabilistic latent semantic analysis algorithm described in Section \ref{sec:gmm}.
\item \texttt{tag}: uses the tag-based algorithm descibed in Section \ref{sec:tag}.
\item \texttt{knn-gplsa}: a hybrid system using both the \texttt{knn} and \texttt{gplsa} systems to produce intermediate outputs, and a neural network as descibed in Section \ref{sec:ann} trained on the development data to fuse these results into a single prediction.
\item \texttt{knn-tag}: a hybrid system using both the \texttt{knn} and \texttt{tag} systems to produce intermediate outputs, and a neural network as descibed in Section \ref{sec:ann} trained on the development data to fuse these results into a single prediction.
\item \texttt{gplsa-tag}:  a hybrid system using both the \texttt{gplsa} and \texttt{tag} systems to produce intermediate outputs, and a neural network as descibed in Section \ref{sec:ann} trained on the development data to fuse these results into a single prediction.
\item \texttt{knn-gplsa-tag}:  a hybrid system using the \texttt{knn}, \texttt{gplsa},  \texttt{tag} systems to produce intermediate outputs, and a neural network as descibed in Section \ref{sec:ann} trained on the development data to fuse these results into a single prediction.
\item \texttt{knn-gplsa-tag-user-5}: a hybrid system using the \texttt{knn}, \texttt{gplsa}, \texttt{tag} and \texttt{user-avg} systems to produce intermediate outputs, and a neural network as descibed in Section \ref{sec:ann} trained on the development data to fuse these results into a single prediction.
\item \texttt{knn-gplsa-tag-user-7}: a hybrid system using the \texttt{knn}, \texttt{gplsa}, \texttt{tag} and \texttt{user-avg} systems to produce intermediate outputs, and a neural network as descibed in Section \ref{sec:ann} trained on the development data to fuse these results into a single prediction, but using 7 nodes in the hidden layer rather than 5.
\item \texttt{hybrid-all-5}:  a hybrid system using the \texttt{knn}, \texttt{gplsa}, \texttt{tag}, \texttt{svd} and \texttt{user-avg} systems to produce intermediate outputs, and a neural network as descibed in Section \ref{sec:ann} trained on the development data to fuse these results into a single prediction, but using 7 nodes in the hidden layer rather than 5.
\item \texttt{hybrid-all-8}:  a hybrid system using the \texttt{knn}, \texttt{gplsa}, \texttt{tag}, \texttt{svd} and \texttt{user-avg} systems to produce intermediate outputs, and a neural network as descibed in Section \ref{sec:ann} trained on the development data to fuse these results into a single prediction, but using 8 nodes in the hidden layer rather than 5.
\end{itemize}
Table \ref{tab:results} summarizes the results from each system. Mean absolute errors and root mean squared errors for the test set are presented graphically in Figures \ref{fig:mae} and \ref{fig:rmse}, respectively.
\begin{table}\label{tab:results}
\centering
\begin{tabular}[h!]{|l|c|c|c|c|}
\hline
\multirow{2}{*}{System} & \multicolumn{2}{|c|}{Development set} &  \multicolumn{2}{|c|}{Test set} \\
\cline{2-5}
& MAE & RMSE & MAE & RMSE \\
\hline
\texttt{user-avg} & 0.7700 & 0.9780 & 0.7701 & 0.9788\\
\texttt{item-avg} & 0.7498 & 0.9616 & 0.7492 & 0.9617\\
\texttt{knn} & \textbf{0.6907} & \textbf{0.905} & 0.6902 & 0.9047 \\
\texttt{svd} & 0.7321 & 0.9887 & 0.7328 & 0.9917 \\
\texttt{gplsa} & 0.7158 & 0.9311 & 0.7161 & 0.9315 \\
\texttt{tag} & 0.7483 & 0.9607 & 0.9626 & 0.7492 \\
\texttt{knn-gplsa} & & & 0.6806 & 0.8773 \\
\texttt{knn-tag} & & & 0.6791 & 0.8750 \\
\texttt{knn-gplsa-tag} & & & 0.6672 & 0.8729 \\
\texttt{knn-gplsa-tag-user-5} & & & 0.6680 & 0.8621 \\
\texttt{knn-gplsa-tag-user-7} & & & 0.6682 & 0.8621\\
\texttt{hybrid-all-5} & & & 0.6681 & 0.8619 \\
\texttt{hybrid-all-7} & & & \textbf{0.6679} & \textbf{0.8617} \\
\hline
\end{tabular}
\caption{Results from each system for test data partitioned from MovieLens 10M dataset}
\end{table}
\begin{figure}
\makebox[\textwidth][c]{\includegraphics[width=1.15\textwidth]{results_mae.png}}
\caption{Mean absolute errors on the test set for each evaluated system}
\label{fig:mae}
\end{figure}
\begin{figure}
\makebox[\textwidth][c]{\includegraphics[width=1.15\textwidth]{results_rmse.png}}
\caption{Root mean square errors on the test set for each evaluated system}
\label{fig:rmse}
\end{figure}
\section{Discussion}
In this report, we describe our attempt to design and implement a system to perform the \textit{predict task} for user-item ratings: the system attempts to predict the rating that would be assigned by a user to an item, given a dataset of previous ratings of many different items by many different users. To evaluate our implementations, we used the MovieLens 10M dataset, a collection of rating and tag application data gathered from the MovieLens website, which is widely used to evaluate systems of this kind.
\par
The most simple approaches to the predict task include taking a user's average rating as a prediction for all ratings made by that user, or taking an item's average rating as a prediction for all ratings made of that item, and these approaches are able to make reasonable predictions, but a greater degree of prediction accuracy can be achieved with more complex methods that operate on data regarding many users and items to make predictions for a given user-item pair. 
\par
We investigated methods including the application of singular value decomposition to the user-item rating matrix, the use of a $k$-nearest-neighbour approach to users with distances between users based on a similarity measure between their ratings vectors, a probabilistic approach modelling user ratings with Gaussian mixture models weighted by inferred latent variables, and a system designed to utilize data regarding user applications of descriptive tags to items to predict the rating behavior of users. Each of these methods performed more accurately than the na\"{\i}ve baseline methods, with the $k$NN-based approach achieving the most accurate results of the standalone systems.
\par
To further improve the accuracy of predictions, we implemented a hybrid approach which uses a small neural network, trained on held-out developent data, to fuse the outputs of individual prediction systems to produce a single prediction. We found that hybrid systems incorporating the results of two or more systems which use different approaches to prediction performed better than any of the standalone systems, suggesting that each system is able to use information present in the data that is not fully utilized by the other systems (in the case of the tag-based system, additional data is also incorporated in the form of the tag applications). Fusing the best-performing standalone system (\texttt{knn}) with either the tag-based or Gaussian mixture model based system produced noticably more accurate results than the \texttt{knn} system alone, and fusing all three produced slightly more accurate results.
\par
Including the \texttt{item-average} baseline in the fusion inputs resulted in a further noticable improvement in accuracy, but we were not able to obtain any significant additional improvement either by including the \texttt{svd} system, or by increasing the number of hidden nodes in the neural network, suggesting that much of the information used by the \texttt{svd} system to compute its predictions is already exploited by the other standalone systems, and that the 5-node neural network provides a sufficiently powerful model for the fusion of the evaluated subsystems.
\par
Our most accurate system (\texttt{hybrid-all-8}) was able to predict the test ratings with a mean absolute error of $0.6679$, compared to the \texttt{item-average} baseline, which had a mean absolute error of $0.7492$. The best standalone system (\texttt{knn}) was able to predict the test ratings with a mean absolute error of $0.6902$. This suggests that a significant improvement in ratings accuracy over simple user averaging can be obtained using methods that are able to take into account information from multiple users and items in the calculation of each prediction, and that further improvements in accuracy can be obtained by fusing two or more dissimilar prediction systems to produce a hybrid system.
\bibliographystyle{plain}
\bibliography{report} 
\newpage
\begin{appendices}
\renewcommand{\thesection}{\Alph{section}}
\renewcommand{\thesubsection}{\arabic{subsection}}
\subsection{Additional Figures}\label{app:fig}
\begin{figure}[h!]
\makebox[\textwidth][c]{\includegraphics[width=1.1\textwidth]{gplsa_classes.png}}
\caption{Chart of number of latent classes vs. RMSE for GPLSA system tested on MovieLens 100k dataset}
\label{fig:gplsa_classes}
\end{figure}
\begin{figure}[h!]
\makebox[\textwidth][c]{\includegraphics[width=0.5\textwidth]{ann.png}}
\caption{Diagrammatic representation of the structure of the artificial neural network used for system fusion}
\label{fig:ann}
\end{figure}

\subsection{Additional Results for Memory-based Systems}\label{app:svd}
The $k$NN and SVD based systems were first tested on MovieLens 100k data set, and the two best-performing systems were selected for evaulation with the MovieLens 1M dataset. The following systems were evaluated:
\begin{itemize}
\item \texttt{knn-prediction}: uses the $k$-nearest-neighbours algorithm descibed in Section \ref{sec:knn}, with $k = 60$
\item \texttt{SVD}: uses singular value decomposition, using Equation \ref{eq:predict_dm} to calculate the predicted rating.
\item \texttt{SVD-user-similarity-weighted-rating}:  uses the singular value decomposition-based algorithm descibed in Section \ref{sec:svd} to calculate similarity scores, using ratings from the 60 most similar users to calculate the predicted rating, using a weighted average by similarity score.
\item \texttt{SVD-user-similarity-SVD-prediction}: uses the singular value decomposition-based algorithm descibed in Section \ref{sec:svd} to calculate similarity scores, using actual and imputed ratings from the 60 most similar users to calculate ratings, using a weighted average by similarity score.
\item \texttt{Pearson-similarity-SVD-prediction}: uses Pearson similarity descibed in Section \ref{sec:knn} to calculate user similarity scores, using actual and imputed ratings from the 60 most similar users to calculate ratings, using a weighted average by similarity score.
\end{itemize}
 Table \ref{tab:Memory_base_100k} summarizes the results from each system.
\begin{table}\label{tab:Memory_base_100k}
\centering
\begin{tabular}[h!]{|l|c|c|c|c|}
\hline
\multirow{2}{*}{System} & \multicolumn{2}{|c|}{100k test set}\\
\cline{2-3}
& MAE & RMSE \\
\hline
\texttt{knn-prediction} & \textbf{0.7439} & \textbf{0.955} \\
\texttt{SVD} &  0.8188 & 1.0278  \\
\texttt{SVD-user-similarity-weighted-rating} & 0.7763 &  0.9956\\
\texttt{SVD-user-similarity-SVD-prediction}  & 0.8304 & 1.0494\\
\texttt{Pearson-similarity-SVD-prediction}  & 0.8063 & 1.0178\\
\hline
\end{tabular}
\caption{Evaluation of memory-based systems for test data from MovieLens 100k dataset by 5-fold evaluation}
\end{table}
\end{appendices}
\end{document}