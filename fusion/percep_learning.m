clear;clc;close all;
%perceptron learning

%dev_data 5 systems
[actual_d,predicted_d1]=load_system_data('.\results_decomp\results_dev.mat');
[~,predicted_d2]=load_system_data('.\results_prob\results_dev.mat');
[~,predicted_d3]=load_system_data('.\results_tag\results_dev.mat');
[~,predicted_d4]=load_system_data('.\results_base\results_dev.mat');
[~,predicted_d5]=load_system_data('.\results_svd\results_dev.mat');

%test_data 5 systems
[actual_t,predicted_t1]=load_system_data('.\results_decomp\results_test.mat');
[~,predicted_t2]=load_system_data('.\results_prob\results_test.mat');
[~,predicted_t3]=load_system_data('.\results_tag\results_test.mat');
[~,predicted_t4]=load_system_data('.\results_base\results_test.mat');
[~,predicted_t5]=load_system_data('.\results_svd\results_test.mat');


input=[predicted_d1;predicted_d2;predicted_d3;predicted_d4;predicted_d5];
input_labels=actual_d;

test=[predicted_t1;predicted_t2;predicted_t3;predicted_t4;predicted_t5];
test_labels=actual_t;

net = fitnet(5);%perceptron learning
view(net)
[net,tr] = train(net,input,input_labels);

testY = net(test);
figure;plotregression(test_labels,testY)
e = test_labels - testY;
figure;ploterrhist(e)

MS_err_N=sqrt(mean((testY-test_labels).^2));
MAE_err_N=mean(abs(testY-test_labels));


