function [actual,predicted ] = load_system_data(file_name )

load(file_name);

actual=ratings(:,3);
actual=cell2mat(actual)';
predicted=ratings(:,4);
predicted=double(cell2mat(predicted))';

end

