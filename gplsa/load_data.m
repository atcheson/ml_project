%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%load ratings in to a marix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [user_movie_ratings, movie_index]=load_data(rating_file, movie_file)

        if exist('rating_limit', 'var') ~= 1
            rating_limit = 1000000;
        end
        
        %get movie index
        [movie_index]=build_movie_index(movie_file);

        fileID = fopen(rating_file,'r');
        ratings = textscan(fileID,'%f%f%f%f', rating_limit, 'Delimiter', {'::'}, 'headerLines', 0);
        fclose(fileID);

        user_movie_ratings = zeros(max(ratings{1}), nnz(movie_index));

        for rating = 1:length(ratings{1})
            movie = movie_index(ratings{2}(rating));
            if(movie ~= 0)
                user_movie_ratings(ratings{1}(rating), movie) = ratings{3}(rating);
            end
        end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Generate movie index to map actual movie ids
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [movie_index]=build_movie_index(movie_file)
   
    fileID = fopen(movie_file,'r');
    movies = textscan(fileID, '%u%q%s',  'Delimiter', {'::'}, 'headerLines', 0);
    fclose(fileID);

    movie_index = zeros(max(movies{1}), 1);
    for index = 1:length(movies{1})
       movie_index(movies{1}(index)) = index; 
    end
    
end