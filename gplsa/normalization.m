function [norMarix, meanUser, stdUser]=normalization(orgMarix,q)
    [maxUsers ,maxMovies] = size(orgMarix);
    
    %Calculate global mean
    edges = [ 0 1 2 3 4 5];
    n=histc(orgMarix,edges,2);
    numRatingUser = n(:,2)+n(:,3)+n(:,4) + n(:,5) + n(:,6);
    sumRatingUser=sum(orgMarix,2);
    sumRating=0;
    countRating=0;
    
    for i=1:maxUsers
        sumRating=sumRating+ sumRatingUser(i);
        countRating=countRating+numRatingUser(i);
         
    end
    globalMean=sumRating/countRating;
         
    %Calculate global standard deviation
    acc = 0;
    count = 0;
    for i =1:maxUsers
        for j=1:maxMovies
            if orgMarix(i,j) ~= 0
               acc = acc + (orgMarix(i,j)-globalMean)^2;
               count = count +1;
            end
        end
    end
    globalVar= acc/count;
        
    %Mean per user
    fac=q*globalMean;
    meanUser = (sum(orgMarix,2)+ fac) ./ (numRatingUser+q);
    
    %Standard deviation per user
     VarUser = 0;
     for i =1:maxUsers
         acc = 0;
         count = 0;
         for j=1:maxMovies
             if orgMarix(i,j) ~= 0
                acc = acc + (orgMarix(i,j)-meanUser(i))^2;
                count = count +1;
             end
         end
          acc=acc + (q*globalVar);
          VarUser(i,:)= acc/(count+q);
      end
      stdUser = sqrt(VarUser);
           
   %normalize ratings
    for i = 1:maxUsers
        for j=1:maxMovies
          if orgMarix(i,j)~=0
             orgMarix(i,j) = (orgMarix(i,j)-meanUser(i)) / stdUser(i);
          end
         
        end
    end
    
    norMarix=orgMarix;
return;