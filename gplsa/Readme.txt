I. INTRODUCTION
    The MATLAb program contained the implementation of Gaussian Probabilistic Latent Semantic Analysis(pLSA) algorithm
for collaborative filtering. This program use 1M dataset from 10M dataset available from MovieLense project 
(http://grouplens.org/datasets/movielens/10m/)

II. CONTENT

    --Files--
    1. run.m          	#Main scrip to start the program. Use to initialize the parameter values.
    2. do_pLSA.m      	#Use to get training results and generate test and development rating files with predicted ratings. 
    3. pLSA.m         	#The main algorithm contains here including init, expectation and maximization steps
    4. load_data.m    	#To read training data file and load data into a matrix. do_pLSA scrip is using this scrip to load 
                   	data to train pLSA model
    5. normalization.m	#To normalize the user ratings. pLSA scrip is using this to normalize user ratings.
    6. guassian.m     	#To model rating values using Gaussian distribution. pLSA script is using this script while training the mode.

    --Directories--
    1. Data          	#Contains data files for train, test and development
    2. Results          #Store results files 
    

III. SETUP/INSTALLATION

    * You must have MATLAB software installed on your computer.
    * Copy and move all Matlab files to the MATLAB work directory.
    * Save your data (train.data, test.data, dev.data, movie.dat) files under 'Data' directory inside the main working folder.
    * To run, open the 'run.m' file and set up parameter.
    * Then you can run the program and results files will be stored in "Results" directory under your main working directory.