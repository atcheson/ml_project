function do_pLSA(numIteration, beta,q,numLatentClass, trainFile, testFile, devFile, movieFile)
   
    %load ratings in to a user*movie marix and movie index to map movie ID
    [ratings,movie_index]=load_data(trainFile, movieFile);
           
    %train pLSA and get expected ratings
    [ExpectedRating]=pLSA(numLatentClass, numIteration, ratings, beta,q);

    %save test data with expected ratings
    if exist('rating_limit', 'var') ~= 1
       rating_limit = 1000000;
    end
    fileID = fopen(testFile,'r');
    test_ratings = textscan(fileID,'%f%f%f%f', rating_limit, 'Delimiter', {'::'}, 'headerLines', 0);
    fclose(fileID);
            
    results_test = cell(length(test_ratings{1}), 4);
            
    for rating=1:length(test_ratings{1})
         movie = movie_index(test_ratings{2}(rating));
            if(movie ~= 0)
                results_test (rating,1) = {test_ratings{1}(rating)};
                results_test (rating,2) = {test_ratings{2}(rating)};
                results_test(rating, 3) = {test_ratings{3}(rating)};
                results_test(rating, 4) = {ExpectedRating(test_ratings{1}(rating),movie)};
            end
    end
            
    save('Results/results_test_1M.mat','results_test');         

    %save development data with expected ratings
    fileID = fopen(devFile,'r');
    dev_ratings = textscan(fileID,'%f%f%f%f', rating_limit, 'Delimiter', {'::'}, 'headerLines', 0);
    fclose(fileID);

    results_dev = cell(length(dev_ratings{1}), 4);

    for rating=1:length(dev_ratings{1})
        movie = movie_index(dev_ratings{2}(rating));
            if(movie ~= 0)
                results_dev (rating,1) = {dev_ratings{1}(rating)};
                results_dev (rating,2) = {dev_ratings{2}(rating)};
                results_dev(rating, 3) = {dev_ratings{3}(rating)};
                results_dev(rating, 4) = {ExpectedRating(dev_ratings{1}(rating),movie)};
            end
   end
            
   save('Results/results_dev_1M.mat','results_dev'); 

return;   




