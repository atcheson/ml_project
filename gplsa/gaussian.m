function p = gaussian(v, mean, sd) 
    
	if sd~=0
        x=(v-mean)/sd;
		p =exp(-(x * x) /2)/(sqrt(2*pi)*sd);
	
	else 
		if abs(v - mean) < 0.00001 
            p = 1;
        else 
            p =0;
        end
	end
	
end
