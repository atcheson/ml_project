close all
clear

%%%%%%%%%%%%%%%%%%%%%%%%%
% Custom input Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%
numIteration = 100;
beta = 1;
q=5;
latentclasses=20;
trainFile='Data/train.dat';
testFile= 'Data/test.dat';
devFile= 'Data/dev.dat';
movieFile= 'Data/movies.dat';

do_pLSA(numIteration,beta,q,latentclasses, trainFile, testFile, devFile,movieFile);