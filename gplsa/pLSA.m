function [ExpectedRating]=pLSA(numLatentClass, numIteration, ratings, beta,q)

    %save number of users and movies
    [users ,movies] = size(ratings);
    
    %save original ratings before normalize
    origRatings=ratings;
    
    %normalize ratings
    [ratings,meanUser, stdUser]=normalization(ratings,q);
    
    %initials values
    [Pzu, M_zi, Std_zi]=paraInitialization(users,movies,numLatentClass, ratings);
    
	for countIter=1:numIteration
        %E step
        [Q]=expectationStep(ratings, numLatentClass, Pzu, M_zi, Std_zi, beta, users, movies,origRatings);		
        %M Step
        [M_zi,Std_zi, Pzu ]=maximizationStep(M_zi, movies, numLatentClass,ratings, Q, Std_zi, Pzu, users, origRatings);	
        %expected rating
        [ExpectedRating]=expectedRating(users,movies, numLatentClass, Pzu, M_zi, meanUser, stdUser);
        %evaluvation
        [RMSE]=evaluvation(users, movies, ExpectedRating, origRatings);
                
		RSMEs(countIter)=RMSE;
        
		%early stopping 
        if countIter > 5
            if abs(RMSEs(countIter-1) - RMSEs(countIter)) < RMSEs(countIter-1)*0.0001
				break;
            end
        end
    end
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialize conditional probabilities for EM 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Pzu, M_zi, Std_zi] = paraInitialization(users,movies,numLatentClass, ratings)
    
    %calculate P(z|u)
    Pzu=zeros(users, numLatentClass);
    for i=1:users
        probs=randperm(numLatentClass);
        summ=sum(probs,2);
        for j=1:numLatentClass
            probs(j)=probs(j)/summ;
        end
        Pzu(i,:)=probs;
    end
     
    %Mean per Item
    for i=1:movies
        acc=0;
        count=0;
        for j=1:users
            if ratings(j,i)~=0
                acc=acc+ratings(j,i);
                count=count+1;
            end                
        end
        if count~=0
            meanMovie(i)=acc/count;
        else
            meanMovie(i)=0;
        end
    end
    
    %Standard deviation per movie
    for i=1:movies
        acc=0;
        count=0;
        for j=1:users
            if ratings(j,i)~=0
                acc=acc+((ratings(j,i)-meanMovie(i))^2);
                count=count+1;
            end
        end
        if count~=0
            stdMovie(i)=sqrt(acc/count);
        else
            stdMovie(i)=0;
        end
    end
    
    % calculate noMovies*noClasses means m(z,i) and standard deviations(z,i)
    M_zi = rand(movies, numLatentClass);
	Std_zi = rand(movies, numLatentClass);
    
    for i=1:movies
        for j=1:numLatentClass
            M_zi(i,j)=meanMovie(i) + (0.01*M_zi(i,j));
            Std_zi(i,j)=stdMovie(i)+(0.01*Std_zi(i,j));
        end
    end
return;
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% E step: compute posterior probabilities of the z  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 function [Q]=expectationStep(ratings, numLatentClass, Pzu, M_zi, Std_zi, beta, users, movies, origRating)
     
     %P(z|u,i)
     Q = zeros(users, movies, numLatentClass);

 	for countUser=1:users
			for countItem=1:movies
				denom = 0;
				enume=0;
                if origRating(countUser,countItem)~=0
                    
					for countLC=1:numLatentClass
                        g=gaussian(ratings(countUser,countItem),M_zi(countItem,countLC),Std_zi(countItem,countLC));
						enume(countLC) = Pzu(countUser,countLC) .* g;
						enume(countLC) = enume(countLC).^beta;
						denom = denom + enume(countLC);
                    end
                                    
					if denom ~=0
                        for countLC=1:numLatentClass
                            Q(countUser,countItem,countLC) = enume(countLC)/denom;
                        end
					else
						Q(countUser,countItem,:)=0;
                    end
                    
                 end 
            end
		end
return;
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% M step: M_zi, Std_zi and P(z|u) obtained by maximizing the expected log-likelihood 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [M_zi,Std_zi, Pzu ]= maximizationStep(M_zi, movies, numLatentClass,ratings, Q, Std_zi, Pzu, users,origRatings)
    
   	%maximizing P(z|u)
	for countUser=1:users
			denom=0;
            enum = 0;
			for countLC=1:numLatentClass
				val=0;
				for countItem = 1 : movies
					if origRatings(countUser,countItem) ~= 0
                       val = val + Q(countUser,countItem,countLC);
					end
                end
                enum(countLC)=val;
                denom = denom + enum(countLC);
			end
            
            if denom ~=0
                for countLC=1:numLatentClass
                    Pzu(countUser,countLC) = enum(countLC)/denom;
                end
            end
    end
		
        
	%Maximizing M_zi
 	for countItem=1:movies
        for countLC=1:numLatentClass
            enum = 0;
            denom = 0;
            for countUser = 1 : users
                if origRatings(countUser,countItem) ~= 0
                    enum = enum + ratings(countUser,countItem)*Q(countUser,countItem,countLC);
                    denom = denom + Q(countUser,countItem,countLC);
                end
            end
            if (denom ~=0)
                M_zi(countItem,countLC) = enum/denom;
            else
                M_zi(countItem,countLC) = 0;
            end
        end
    end
				
	%Calculate Std_zi
	for countItem=1:movies
        for countLC=1:numLatentClass
            enume = 0;
            denom =0;
            for countUser = 1 : users
                if origRatings(countUser,countItem) ~= 0
                    enume = enume + ((ratings(countUser,countItem)-M_zi(countItem,countLC))^2)*Q(countUser,countItem,countLC);
                    denom = denom +  Q(countUser,countItem,countLC);
                end
            end
            if(denom>0)
                Std_zi(countItem,countLC) = sqrt(enume/denom);
            else
                Std_zi(countItem,countLC) = 0;
            end
        end
    end
    
return;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  calculate expected ratings using the model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ExpectedRating]=expectedRating(users,movies, numLatentClass, Pzu, M_zi, meanUser, stdUser)
         		
		ExpectedRating = zeros(users,movies);
		
		for countUser=1:users
			for countItem=1:movies
				val = 0;
				for countLC = 1:numLatentClass
					val = val + Pzu(countUser,countLC)*M_zi(countItem, countLC);
				end
				ExpectedRating(countUser,countItem) =meanUser(countUser)+ (stdUser(countUser)*val);
			end
		end
return;

%%%%%%%%%%%%%%%%%%
%  calculate RMSE
%%%%%%%%%%%%%%%%%%
function [RMSE]=evaluvation(users, movies, ExpectedRating, origRatings)
     MSE = 0;
     numRating = 0;
                
    for countUser=1:users
        for countItem=1:movies
            if origRatings(countUser, countItem) ~= 0
                numRating = numRating + 1;
                MSE = MSE +  (origRatings(countUser,countItem)-ExpectedRating(countUser,countItem))^2;
            end
        end
    end
		
    RMSE = sqrt(MSE/numRating);

return;
        
	
	