function [ user_sim_mat] = cosine_svd_sim(U)
%Calculating user similarity through SVD U user-interest matrix

[r c]=size(U);

user_sim_mat=zeros(size(U));
for j=1:r
    for i=1:c
        if j==i
            user_sim_mat(j,i)=-1;%Say that same user similarity =-1
        else
                
        user_sim_mat(j,i)= (U(j,:)*U(i,:)')/...
            (norm(U(j,:))*norm(U(i,:)));
        end
    end
end

end