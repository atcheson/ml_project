clear;clc;
%Colloboratiuve filtering using K nearest neighbour (Pearson Siilarity, Weighted averaging)

[training_data,user_list,user_label,item_list,item_label] =load_data_train_1M('./ml-1M/train.dat');
testing={'./ml-1M/dev.dat','./ml-1M/test.dat'};
saving={'./ml-1M/results_base_dev.mat','./ml-1M/results_base_test.mat'};

training_data_pad=item_averaging(training_data); 

for j=1:length(testing)
    
[users,items,actual_rating] = load_data_test_1M(testing{j});
no_nearest_users=60;

pred_rating=zeros(size(users));
parfor i=1:length(users)
    
user=users(i);
item=items(i);
I = find(item_list==item,1);
J = find(user_list==user,1);
temp_user=user_label(J);

    if isempty(I)
        pred_rating(i,1) =0;
    else

    temp_item=item_label(I);

     pred_rating(i,1) =training_data_pad(temp_user,temp_item);
   
    end
fprintf('%d : done. %d\n',j,i);
end
pred_rating(isnan(pred_rating)) = 0 ;
ratings=[users,items,actual_rating,pred_rating];
ratings = num2cell(ratings);

save(saving{j},'ratings');

RMS_err(j,1)=sqrt(mean((pred_rating-actual_rating).^2));
MAE_err(j,1)=mean(abs(pred_rating-actual_rating));

end
