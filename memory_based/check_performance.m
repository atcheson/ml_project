clear;clc
%compute RMSE and MAE seperately for saved result files

load('./ml-1M/results_dev.mat');
%load('./ml-1M/results_test.mat');
actual=ratings(:,3);
actual=double(cell2mat(actual));
predicted=ratings(:,4);
predicted=cell2mat(predicted);
predicted(isnan(predicted)) = 0 ;

MS_err=sqrt(mean((predicted-actual).^2));
MAE_err=mean(abs(predicted-actual));

