function [U,S,T] = col_svd(training_data)
%SVD decomposition of training data

mu=zeros(1,size(training_data,2));
for i=1:size(training_data,2)
    [~,~,item]=find(training_data(:,i));
    if (sum(item)~=0)
        mu(1,i)=mean(item);
    end
end

[row,col]=find(~training_data);
temp=zeros(size(training_data));

for i=1:length(row)
  temp(row(i),col(i))=1;   
end

pad=bsxfun(@times,temp,mu);
training_data_pad=training_data+pad;


[U,S,T] = svd(training_data_pad) ;

end

