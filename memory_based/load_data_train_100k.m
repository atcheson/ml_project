function [data_matrix,user_list,user_label,item_list,item_label] = load_data_train_100k(dataList)

fid = fopen(dataList, 'r');
C = textscan(fid, '%f%f%f%f%f%f%f%f','Delimiter',':');
fclose(fid);

user_list=C{:,1};
[~, iaa, user_label] = unique(user_list,'stable');

item_list=C{:,2};
[W, ia, item_label] = unique(item_list,'stable');
no_users=length(iaa);
no_items=length(ia);
data_matrix= zeros(no_users,no_items);

for i=1: size(C{1,1},1)
    data_matrix(user_label(i),item_label(i))=C{1,3}(i,1);
end

end