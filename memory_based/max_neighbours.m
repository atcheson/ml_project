function [sim,k_neighbours] = max_neighbours(training_data,user_sim_mat,user,item,no_nearest_users)
%SVD k neighbour selection thorugh similarity matrix

users_voted_item=find(training_data(:,item));
sim_vec=user_sim_mat(:,user);

sim_vec_k=sim_vec(users_voted_item(:,1),:);

[sim,k_neighbours]= sort(sim_vec_k,'descend');
k_neighbours=users_voted_item(k_neighbours,:);


[r, ~] = find(sim > 0);
sim=sim(r,1);
k_neighbours=k_neighbours(r,1);

if length(k_neighbours)>no_nearest_users
    k_neighbours=k_neighbours(1:no_nearest_users,1);
    sim=sim(1:no_nearest_users,1);
end

end