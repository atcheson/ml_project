function [users,items,actual_rating] = load_data_test_100k(dataList)

fid = fopen(dataList, 'r');
C = textscan(fid, '%f%f%f%f');
fclose(fid);

users=C{1,1};
items=C{1,2};
actual_rating=C{1,3};

end