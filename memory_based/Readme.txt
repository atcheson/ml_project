Memory Based Collaborative filtering

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$$$$$$ Main codes $$$$$$
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
****MovieLens 100k data set****
Place the 5 fold cross validation data files inside the folder "ml-100k" (u1.base-u5.base, u1.test- u5.test)

1)Baseline_100k.m #Baseline System calculates RMSE and MSE for simple Item averaging
2)knn_prediction_100k.m # Collaborative filtering using K nearest neighbour (Pearson Similarity, Weighted averaging) Save the 5 cross validation result files inside folder ml-100k
3)knn_prediction_error.m # Collaborative filtering using K nearest neighbour (Pearson Similarity, Weighted averaging) Compute RMSE and MSE error directly

**SVD predictions**
4)svd_pred.m # Direct rating prediction though U user-interest matrix after calculating SVD
5)svd_sim_per_pred.m # used SVD user similarity calculation for k neighbours and rating prediction through weighted averaging
6)svd_sim_svd_pred.m # used SVD user similarity calculation for k neighbours and rating prediction through SVD 
7)per_sim_svd_pred.m # used Pearson user similarity calculation for k neighbours and rating prediction through SVD 

****MovieLens 10M data set****
Place "train.dat", "test.dat", "dev.dat" data files inside the folder ml-1M folder

8)Baseline_1M.m #Baseline System calculates RMSE and MSE for simple Item averaging
9)knn_prediction_1M.m # Collaborative filtering using K nearest neighbour (Pearson Similarity, Weighted averaging)
10)svd_sim_per_pred.m # used SVD user similarity calculation for k neighbours and rating prediction through weighted averaging

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$$$$$$ Supportive codes $$$$$$
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

11)check_performance.m # compute RMSE and MAE separately for saved result files
12)item_averaging.m # matrix imputation through item averaging (Matrix padding)
13)cosine_sim.m # Compute cosine similarity between users
14)cosie_svd_sim.m # compute cosine similarity for user-interest matrix U
15)col_svd.m # compute SVD transformation for rating matrix 
16)pearson_sim.m # calculate Pearson similarity between users
17)max_neighbours.m # calculate k nearest neighbours from user similarity matrix
18)presict_rating # weighted rating prediction
19)load_data.m # simple train data loading for 100k dataset
20)load_data_train_100k.m # loading  training data files in 100k dataset
21)load_data_test_100k.m # loading  test data files in 100k dataset
22)load_data_train_1M.m # loading  training data files in 1M dataset
23)load_data_train_1M.m # loading  test data files in 1M dataset