function [data_matrix] = load_data(dataList)

fid = fopen(dataList, 'r');
C = textscan(fid, '%f%f%f%f');
fclose(fid);

no_users=943;
no_items=1682;
data_matrix= zeros(no_users,no_items);

for i=1: size(C{1,1},1)
    data_matrix(C{1,1}(i,1),C{1,2}(i,1))=C{1,3}(i,1);
end

end