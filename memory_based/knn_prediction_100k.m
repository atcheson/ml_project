clear;clc;
%# Colloboratiuve filtering using K nearest neighbour (Pearson Siilarity, Weighted averaging) Save the 5 cross validation result files inside folder ml-100k

training={'./ml-100k/u1.base','./ml-100k/u2.base','./ml-100k/u3.base','./ml-100k/u4.base',...
    './ml-100k/u5.base'};
testing={'./ml-100k/u1.test','./ml-100k/u2.test','./ml-100k/u3.test','./ml-100k/u4.test',...
    './ml-100k/u5.test'};
saving={'./ml-100k/results_u1.mat','./ml-100k/results_u2.mat','./ml-100k/results_u3.mat',...
    './ml-100k/results_u4.mat','./ml-100k/results_u5.mat'};

for j=1:length(testing)
    
[training_data,user_list,user_label,item_list,item_label] =load_data_train_100k(training{j});
[users,items,actual_rating] = load_data_test_100k(testing{j});
no_nearest_users=60;

pred_rating=zeros(size(users));
parfor i=1:length(users)
    
user=users(i);
item=items(i);
I = find(item_list==item,1);
J = find(user_list==user,1);
temp_user=user_label(J);

    if isempty(I)
        pred_rating(i,1) =0;
    else

    temp_item=item_label(I);

%[sim,k_neighbours] = cosine_sim(training_data,user,item,no_nearest_users);
    [sim,k_neighbours] = pearson_sim(training_data,temp_user,temp_item,no_nearest_users);

     pred_rating(i,1) = predict_rating(training_data,temp_user,temp_item,sim,k_neighbours);
    end
fprintf('%d : done. %d\n',j,i);
end
pred_rating(isnan(pred_rating)) = 0 ;
ratings=[users,items,actual_rating,pred_rating];
ratings = num2cell(ratings);

save(saving{j},'ratings');

end

