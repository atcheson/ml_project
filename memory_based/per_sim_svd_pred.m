clear;clc;
%used pearson user similarity calculation for k neighbours and rating prediction through SVD 

training={'./ml-100k/u1.base','./ml-100k/u2.base','./ml-100k/u3.base','./ml-100k/u4.base',...
    './ml-100k/u5.base'};
testing={'./ml-100k/u1.test','./ml-100k/u2.test','./ml-100k/u3.test','./ml-100k/u4.test',...
    './ml-100k/u5.test'};
RMS_err=zeros(length(training),1);
MAE_err=zeros(length(training),1);
for j=1:length(training)

training_data=load_data(training{j});
testing_data=load_data(testing{j});
[U,S,T] = col_svd(training_data);

%[user_sim_mat] = cosine_svd_sim(U);%user similarity matrix
[users,items,actual_rating] = find(testing_data);
no_nearest_users=60;
temp=U*S*T';

pred_rating=zeros(size(users));
parfor i=1:length(users)
    
user=users(i);
item=items(i);

%[sim,k_neighbours] = max_neighbours(training_data,user_sim_mat,user,item,no_nearest_users);
[sim,k_neighbours] = pearson_sim(training_data,user,item,no_nearest_users);
  
user_rat =temp(k_neighbours,item);

    if sum(user_rat)== 0
        pred_rating(i,1) =0;
    else
        pred_rating(i,1) = mean(user_rat);
    end

fprintf('%d : done. %d\n',j,i);
end

RMS_err(j,1)=sqrt(mean((pred_rating-actual_rating).^2));
MAE_err(j,1)=mean(abs(pred_rating-actual_rating));

end

K_fold_RMS_err=mean(RMS_err);
K_fold_MAE_err=mean(MAE_err);
