function [ sim,k_neighbours] = cosine_sim(training_data,user,item,no_nearest_users)
%Calculating cosine similarity

users_voted_item=find(training_data(:,item));
%users_ratings=training_data(users_voted_item,:);

similarity=0*ones(size(training_data,1),1); % dissimilar=0, since ratings are only positve
for i=1:length(users_voted_item)
    
    similarity(users_voted_item(i,1),1)= (training_data(user,:)*training_data(users_voted_item(i,1),:)')/...
        (norm(training_data(user,:))*norm(training_data(users_voted_item(i,1),:)));
end

   [max_sim ,max_neighbours]= sort(similarity,'descend');
   k_neighbours=max_neighbours(1:no_nearest_users,1);
   sim=max_sim(1:no_nearest_users,1);
end