function [users,items,actual_rating] = load_data_test_1M(dataList)

fid = fopen(dataList, 'r');
C = textscan(fid, '%f%f%f%f%f%f%f%f','Delimiter',':');
fclose(fid);

users=C{1,1};
items=C{1,3};
actual_rating=C{1,5};

end