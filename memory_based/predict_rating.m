function [rating] = predict_rating(training_data,user,item,sim,k_neighbours)
%Rating Prediction (Weighted)
%eqn 2.7

users_ratings=training_data(k_neighbours,item);

avg_us_ratings=zeros(length(k_neighbours),1);
var_us_ratings=zeros(length(k_neighbours),1);
for i=1:length(k_neighbours)
[~,~,us_ratings]=find(training_data(k_neighbours(i),:));
avg_us_ratings(i,1)=mean(us_ratings);
var_us_ratings(i,1)=std(us_ratings);
end

[~,~,u_ratings]=find(training_data(user,:));
avg_u_ratings=mean(u_ratings);
var_u_rating=std(u_ratings);

mean_var_norm_us_rating=(users_ratings-avg_us_ratings)./var_us_ratings;

%rating=sum(users_ratings)./length(k_neighbours);%Aggregration method 1 ref 
%rating=users_ratings'*sim /abs(sum(sim)); %Aggregration method 2 ref 
rating=avg_u_ratings+var_u_rating*mean_var_norm_us_rating'*sim /abs(sum(sim)); %Aggregration method 3 ref 

end

