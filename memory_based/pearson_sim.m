function [ sim,k_neighbours] = pearson_sim(training_data,user,item,no_nearest_users)
%Person similarity calculation

users_voted_item=find(training_data(:,item));

similarity=0*ones(size(training_data,1),1); % dissimilar=0
for i=1:length(users_voted_item)
    
    u=training_data(user,:);
    v=training_data(users_voted_item(i,1),:);
    [~,~,u1]=find(u);
    [~,~,v1]=find(v);

    [~, UnV]=find(u.*v);
    u2=u(UnV);
    v2=v(UnV);

    u3=bsxfun(@minus,u2,mean(u1));
    v3=bsxfun(@minus,v2,mean(v1));
    
    if (norm(u3)*norm(v3))~=0
    similarity(users_voted_item(i,1),1)=u3*v3'/(norm(u3)*norm(v3));
    end
end
    [k_neighbours, ~] = find(similarity > 0);
    sim=similarity(k_neighbours,1);
    
    if length(sim)>no_nearest_users
   [max_sim ,max_neighbours]= sort(sim,'descend');
   max_neighbours=k_neighbours(max_neighbours,:);
   k_neighbours=max_neighbours(1:no_nearest_users,1);
   sim=max_sim(1:no_nearest_users,1);
    end
end