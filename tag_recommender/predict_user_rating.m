function [ rating ] = predict_user_rating(user_id, movie_id, user_index, movie_index, user_average_rating, movie_tag_similarity, movie_tag, normalized_user_tag_prefs)
% PREDICT_USER_RATING Using the ouput of BUILD_TAGOMMENER, predict a user's rating for a given movie
%   Valid ratings are between 1 and 5
%   If the system cannot make a prediction for the supplied (user, movie) pair, a zero rating is returned

    tags_to_use = 30; % The maximum number of tags (with highest similarity to the relevant movie) used to predict a rating

    if user_id > length(user_index) || movie_id > length(movie_index)
        rating = 0;
        return
    end
    rating = predict_from_internal_ids(user_index(user_id), movie_index(movie_id), tags_to_use, user_average_rating, movie_tag_similarity, movie_tag, normalized_user_tag_prefs);
end

