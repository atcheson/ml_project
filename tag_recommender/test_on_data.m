function [ results ] = test_on_data(train_ratings_file, test_ratings_file, tag_file, movie_file)
% TEST_ON_DATA  Read the supplied files, train a tag recommender system on the training data, and then make predictions for each rating in the test data.
%   Results are returned in the form of a cell array with columns: user_id, movie_id, correct_rating, predicted_rating
    [ user_index, movie_index, user_average_rating, movie_tag_similarity, movie_tag, normalized_user_tag_prefs ] = build_tagommender(train_ratings_file, tag_file, movie_file);
    
    fileID = fopen(test_ratings_file,'r');
    test_ratings = textscan(fileID,'%u%u%f%u', 'Delimiter', {'::'}, 'headerLines', 0);
    fclose(fileID);
    
    num_test_ratings = length(test_ratings{1});
    results = cell(num_test_ratings, 4);
    
    fileID = fopen(train_ratings_file,'r');
    train_ratings = textscan(fileID,'%u%u%f%u', 'Delimiter', {'::'}, 'headerLines', 0);
    fclose(fileID);
    user_avg_ratings = zeros(max(train_ratings{1}), 2);
    for row = 1:length(train_ratings{1})
        user_id = train_ratings{1}(row);
        rating = train_ratings{3}(row);
        user_avg_ratings(user_id, 1) = ((user_avg_ratings(user_id, 1)*user_avg_ratings(user_id, 2))+double(rating))/(user_avg_ratings(user_id, 2)+1);
        user_avg_ratings(user_id, 2) = user_avg_ratings(user_id, 2) + 1;
    end
    user_avg_ratings = user_avg_ratings(:, 1);
    total_avg_rating = mean(user_avg_ratings);
    
    for test_rating = 1:num_test_ratings
        movie_id = test_ratings{2}(test_rating);
        user_id = test_ratings{1}(test_rating);
        correct_rating = test_ratings{3}(test_rating);
        predicted_rating = predict_user_rating(user_id, movie_id, user_index, movie_index, user_average_rating, movie_tag_similarity, movie_tag, normalized_user_tag_prefs);
        
        if predicted_rating == 0
            if user_id > length(user_avg_ratings)
                predicted_rating = total_avg_rating;
            else
                predicted_rating = user_avg_ratings(user_id);
            end
        end
        
        results{test_rating, 1} = user_id;
        results{test_rating, 2} = movie_id;
        results{test_rating, 3} = correct_rating;
        results{test_rating, 4} = predicted_rating;
    end
end

