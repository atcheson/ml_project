function [ user_index, movie_index, user_average_rating, movie_tag_similarity, movie_tag, normalized_user_tag_prefs ] = build_tagommender(rating_file, tag_file, movie_file)
% BUILD_TAGOMMENDER  Generate the matrices needed to perform user-movie rating prediction using a tag-based method
%   The outputs of this function are designed to be supplied to PREDICT_USER_RATING

    minimum_movies = 5; % Tags with fewer than this number of movies will be iteratively pruned from the data before processing
    minimum_tags = 5; % Movies with fewer than this number of tags will be iteratively pruned from the data before processing
    minimum_ratings = 5; % Users with fewer than this number of ratings will be iteratively pruned from the data before processing
    
    run('scripts/build_movie_index.m'); 
    run('scripts/load_movie_tag_matrix.m');
    run('scripts/prune_movie_tag_matrix.m');
    run('scripts/load_ratings_matrix.m');
    run('scripts/prune_users');
    run('scripts/calculate_tag_relevance.m'); 
    run('scripts/calculate_user_tag_prefs.m'); 
    run('scripts/normalize_user_tag_prefs.m');
    run('scripts/calculate_movie_tag_similarity.m');
end

