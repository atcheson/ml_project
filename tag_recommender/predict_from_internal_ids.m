function [ rating ] = predict_from_internal_ids(user, movie, tags_to_use, user_average_rating, movie_tag_similarity, movie_tag, normalized_user_tag_prefs)
% PREDICT_FROM_INTERNAL_IDS  Predict a user's rating for a given movie, using the internally-generated user and movie ID numbers, rather than those present in the original data
%   Used for internal testing only; an end-user should never need to call this function.
    rating = 0;
    
    if user == 0 || movie == 0
        return;
    end
    
    similarity_vec = movie_tag(movie, :);
    tags_for_movie = find(similarity_vec ~= 0);
    
    eligible_tags = [];
    for nn = 1:length(tags_for_movie)
        tag = tags_for_movie(nn);
        similarity = movie_tag_similarity(movie, tag);
        if similarity > 0
            eligible_tags = [eligible_tags; [tag, similarity]];
        end
    end
    
    if isempty(eligible_tags) || tags_to_use == 0
        return
    end
    
    eligible_tags = sortrows(eligible_tags, -2);
    
    if size(eligible_tags, 1) > tags_to_use
        eligible_tags = eligible_tags(1:tags_to_use, :);
    end

    numerator_sum = 0;
    denominator_sum = 0;
    for nn = 1:size(eligible_tags, 1)
        tag = eligible_tags(nn, 1);
        similarity = eligible_tags(nn, 2);
        ntp = normalized_user_tag_prefs(user, tag);
        
        numerator_sum = numerator_sum + (similarity * ntp);
        denominator_sum = denominator_sum + similarity;
    end
    
    if denominator_sum == 0
        return;
    end
    
    rating = (numerator_sum/denominator_sum) + user_average_rating(user);
    rating = min(rating, 5);
    rating = max(rating, 1);
end

