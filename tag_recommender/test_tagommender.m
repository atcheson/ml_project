function [mae, predictions] = test_tagommender(rating_file, tag_file, movie_file, rating_limit, partitions, iterations)
% TEST_TAGOMMENDER  Test the tag recommender system on the supplied data with M rounds of N-fold cross validation, where partitions = N and iterations = M
%   Results are returned as mae (a vector of mean average errors for each iteration) and predictions (pairs of correct and predicted ratings for each iteration)
%   Used for internal testing only.

    minimum_movies = 5; % Tags with fewer than this number of movies will be iteratively pruned from the data before processing
    minimum_tags = 5; % Movies with fewer than this number of tags will be iteratively pruned from the data before processing
    minimum_ratings = 5; % Users with fewer than this number of ratings will be iteratively pruned from the data before processing
    tags_to_use = 30; % The maximum number of tags (with highest similarity to the relevant movie) used to predict a rating
    
    run('scripts/build_movie_index.m'); 
    run('scripts/load_movie_tag_matrix.m');
    run('scripts/prune_movie_tag_matrix.m');
    run('scripts/load_ratings_matrix.m');
    run('scripts/prune_users');
    
    full_ratings_matrix = user_movie_ratings;
    ratings = find(user_movie_ratings ~= 0);
    ratings = ratings(randperm(length(ratings)));
    
    predictions = cell(iterations, 1);
    mae = zeros(iterations, 1);
    
    for iter = 1:iterations
        partition_length = floor(length(ratings)/partitions);
        test_ratings_indices = ratings(partition_length*(iter-1)+1:(partition_length*(iter)));
        user_movie_ratings = full_ratings_matrix;
        user_movie_ratings(test_ratings_indices) = 0;
        
        predictions{iter} = zeros(length(test_ratings_indices), 2);
        
        test_ratings = zeros(length(test_ratings_indices), 3);
        [I, J] = ind2sub(size(full_ratings_matrix), test_ratings_indices);
        test_ratings(:, 1) = I;
        test_ratings(:, 2) = J;
        test_ratings(:, 3) = full_ratings_matrix(test_ratings_indices);
        
        run('scripts/calculate_tag_relevance.m'); 
        run('scripts/calculate_user_tag_prefs.m'); 
        run('scripts/normalize_user_tag_prefs.m');
        run('scripts/calculate_movie_tag_similarity.m');
        
        for nn = 1:size(test_ratings, 1)
            test_user = test_ratings(nn, 1);
            test_movie = test_ratings(nn, 2);
            
            predicted_rating = predict_from_internal_ids(test_user, test_movie, tags_to_use, user_average_rating, movie_tag_similarity, movie_tag, normalized_user_tag_prefs);
            
            predictions{iter}(nn, 1) = test_ratings(nn, 3);
            predictions{iter}(nn, 2) = predicted_rating;
        end
        
        errors = abs(predictions{iter}(:, 1) - predictions{iter}(:, 2));
        mae(iter) = mean(errors);
        
        msg = ['Completed iteration ', num2str(iter), ' with MAE = ', num2str(mae(iter))];
        disp(msg);
    end
end

