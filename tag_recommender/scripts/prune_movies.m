% Prune the movie-tag matrix to exclude movies with fewer than minimum_tags tags, and modify the
% movie index to remove pruned movies and ensure that the remaining movies still have contiguous
% index numbers for matrix addressing.

keep_movies = [];
delete_movies = [];

if exist('minimum_tags', 'var') ~= 1
    minimum_tags = 5;
end

for movie = 1:size(movie_tag, 1)
    if nnz(movie_tag(movie, :)) >= minimum_tags
        keep_movies = [keep_movies; movie];
    else
        delete_movies = [delete_movies; movie];
    end
end

new_movie_index = zeros(size(movie_index));
for original_movie_id = 1:length(movie_index)
    if ismember(movie_index(original_movie_id), keep_movies)
        new_movie_index(original_movie_id) = find(keep_movies == movie_index(original_movie_id), 1);
    end
end

movie_index = new_movie_index;
movie_tag(delete_movies, :) = [];