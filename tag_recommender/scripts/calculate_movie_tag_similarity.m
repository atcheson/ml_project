% Calculates the movie-tag similarity matrix

num_movies = size(movie_tag_relevance, 1);
num_tags = size(movie_tag_relevance, 2);
users_rated = cell(num_movies, 1);

user_movie_ratings(user_movie_ratings == 0) = NaN;
user_average_rating = nanmean(user_movie_ratings, 2);
user_movie_ratings(isnan(user_movie_ratings)) = 0;

for movie = 1:num_movies
   movie_ratings = user_movie_ratings(:, movie);
   users_rated{movie} = find(movie_ratings ~= 0);
end

movie_tag_similarity = zeros(num_movies, num_tags);
for movie = 1:num_movies
    for tag = 1:num_tags
        users = users_rated{movie};
        
        numerator_sum = 0;
        denominator_left_sum = 0;
        denominator_right_sum = 0;
        
        for nn = 1:length(users)
            user = users(nn);

            rating_term = user_movie_ratings(user, movie) - user_average_rating(user);
            preference_term = normalized_user_tag_prefs(user, tag);
            
            numerator_sum = numerator_sum + (rating_term * preference_term);

            denominator_left_sum = denominator_left_sum + rating_term^2;
            denominator_right_sum = denominator_right_sum + preference_term^2;
        end
        
        denominator = denominator_left_sum^(1/2) * denominator_right_sum^(1/2);
        if denominator ~= 0
            movie_tag_similarity(movie, tag) = numerator_sum / denominator;
        end
    end
end