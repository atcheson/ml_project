% Prune the user-movie ratings matrix to exclude users who have made fewer than minimum_ratings ratings,
% and update the user index to ensure that the remaining users have contiguous index numbers.

keep_users = [];
delete_users = [];

if exist('minimum_ratings', 'var') ~= 1
    minimum_ratings = 5;
end

for user = 1:size(user_movie_ratings, 1)
    if nnz(user_movie_ratings(user, :)) >= minimum_ratings
        keep_users = [keep_users; user];
    else
        delete_users = [delete_users; user];
    end
end

new_user_index = zeros(size(user_index));
for original_user_id = 1:length(user_index)
    if ismember(user_index(original_user_id), keep_users)
        new_user_index(original_user_id) = find(keep_users == user_index(original_user_id), 1);
    end
end

user_index = new_user_index;
user_movie_ratings(delete_users, :) = [];