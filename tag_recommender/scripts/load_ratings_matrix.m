% Loads the user-movie ratings matrix from the ratings data file, and generates the user index to map
% user IDs present in the file to internal user IDs (to deal with potential sparsity of user IDs in the data file)

if exist('rating_file', 'var') ~= 1
    rating_file = '../ml-10M100K/ratings.dat';
end

fileID = fopen(rating_file,'r');
ratings = textscan(fileID,'%u%u%f%u', 'Delimiter', {'::'}, 'headerLines', 0);
fclose(fileID);

user_movie_ratings = zeros(max(ratings{1}), nnz(movie_index));

for rating = 1:length(ratings{1})
    movie = movie_index(ratings{2}(rating));
    if(movie ~= 0)
        user_movie_ratings(ratings{1}(rating), movie) = ratings{3}(rating);
    end
end

user_index = (1:max(ratings{1}))';