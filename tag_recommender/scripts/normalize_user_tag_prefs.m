% Normalizes the user-tag preference matrix by first normalizing each per-tag mean to zero and each
% per-tag standard deviation to one, then normalizing each per-user mean to zero.

normalized_user_tag_prefs = user_tag_prefs;

normalized_user_tag_prefs(normalized_user_tag_prefs == 0) = NaN;
tag_means = repmat(nanmean(normalized_user_tag_prefs, 1), size(normalized_user_tag_prefs, 1), 1);
normalized_user_tag_prefs(isnan(normalized_user_tag_prefs)) = 0;
normalized_user_tag_prefs = normalized_user_tag_prefs - tag_means;
clear('tag_means');

normalized_user_tag_prefs(user_tag_prefs == 0) = NaN;
tag_std = repmat(nanstd(normalized_user_tag_prefs, 0, 1), size(normalized_user_tag_prefs, 1), 1);
normalized_user_tag_prefs(isnan(normalized_user_tag_prefs)) = 0;
normalized_user_tag_prefs = normalized_user_tag_prefs./tag_std;
clear('tag_std');

normalized_user_tag_prefs(user_tag_prefs == 0) = NaN;
user_means = repmat(nanmean(normalized_user_tag_prefs, 2), 1, size(normalized_user_tag_prefs, 2));
normalized_user_tag_prefs(isnan(normalized_user_tag_prefs)) = 0;
normalized_user_tag_prefs = normalized_user_tag_prefs - user_means;
clear('user_means');

normalized_user_tag_prefs(user_tag_prefs == 0) = 0;
