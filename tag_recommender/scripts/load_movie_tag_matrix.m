% Loads the movie-tag application matrix from the tag data file. Tags are
% considered identical if they are identical when case and non-alphanumeric characters are ignored.

if exist('tag_file', 'var') ~= 1
    tag_file = '../ml-10M100K/tags.dat';
end

fileID = fopen(tag_file','r');
tags = textscan(fileID,'%u%u%q%u', 'Delimiter', {'::'}, 'headerLines', 0);
fclose(fileID);

tag_index = containers.Map;

% Index the tags to give each unique tag its own column number
current_index = 1;
max_movie_id = nnz(movie_index); 
for tag_num = 1:length(tags{1})
    tag = lower(regexprep(tags{3}{tag_num},'[^a-zA-Z0-9]',''));
    tags{3}{tag_num} = tag;

    if ~isKey(tag_index, tag)
        tag_index(tag) = current_index;
        current_index = current_index + 1;
    end
end

unique_tags = current_index - 1;
movie_tag = zeros(max_movie_id, unique_tags);

for tag_num = 1:length(tags{1})
    tag = tag_index(tags{3}{tag_num});
    movie = movie_index(tags{2}(tag_num));
    
    movie_tag(movie, tag) = movie_tag(movie, tag) + 1;
end