num_users = size(user_movie_ratings, 1);
num_tags = size(movie_tag_relevance, 2);
tagged_movies = cell(num_tags, 1);

hyper_mean_mean = 3.6;
hyper_mean_std = 1.0;
hyper_std_mean = 2.0;
hyper_std_std = 1.0;

num_iter = 100;

mu_term = @(muu) normpdf(muu, hyper_mean_mean, hyper_mean_std);
sig_term = @(sig) normpdf(sig, hyper_std_mean, hyper_std_std);

user_movie_ratings(user_movie_ratings == 0) = NaN;
user_average_rating = nanmean(user_movie_ratings, 2);
user_std_rating = nanstd(user_movie_ratings, 1, 2);
user_std_rating(user_std_rating == 0) = 0.000001;
user_movie_ratings(isnan(user_movie_ratings)) = 0;

p_rmusig_comp = @(muu, sig, tag, user, movie) (...
    normpdf(user_movie_ratings(user, movie), muu, sig)*movie_tag_relevance(movie, tag) ...
    + normpdf(user_movie_ratings(user, movie), user_average_rating(user), user_std_rating(user))*(1 - movie_tag_relevance(movie, tag)) ...
    );

for tag = 1:num_tags
    tag_relevance = movie_tag_relevance(:, tag);
    tagged_movies{tag} = find(tag_relevance ~= 0);
end

user_tag_prefs = zeros(num_users, num_tags);
for user = 1:num_users
    for tag = 1:num_tags
        movies = tagged_movies{tag};
        eligible_movies = [];
        for nn = 1:length(movies)
            movie = movies(nn);
            if user_movie_ratings(user, movie) ~= 0
                eligible_movies = [eligible_movies;  movie];
            end
        end
        
        if isempty(eligible_movies)
            continue;
        end
        
        p_rmusig_comp_ut = @(muu, sig, movie) p_rmusig_comp(muu, sig, tag, user, movie);
        p_rmusig = @(muu, sig) prod(arrayfun(p_rmusig_comp_ut, repmat(muu, length(eligible_movies), 1), repmat(sig, length(eligible_movies), 1), eligible_movies));
        pdf = @(musig) p_rmusig(musig(1), musig(2))*mu_term(musig(1))*sig_term(musig(2));
        proprnd = @(musig) normrnd(musig, 1);
        
        smpl = mhsample([hyper_mean_mean, hyper_std_mean], num_iter, 'pdf', pdf, 'proprnd', proprnd, 'symmetric', 1);
        
        user_tag_prefs(user, tag) = mean(smpl(:, 1));
    end
end
