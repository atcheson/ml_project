% Calculates the movie-tag relevance measure (in this implementation, the similarity of tag t to movie m is 
% just the proportion of total tag applications to movie m that are of tag t)

movie_tag_relevance = diag(1./sum(movie_tag, 2))*movie_tag;