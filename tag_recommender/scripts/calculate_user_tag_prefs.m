% Calculates the user-tag preference matrix

num_users = size(user_movie_ratings, 1);
num_tags = size(movie_tag_relevance, 2);
tagged_movies = cell(num_tags, 1);

for tag = 1:num_tags
    tag_relevance = movie_tag_relevance(:, tag);
    tagged_movies{tag} = find(tag_relevance ~= 0);
end

user_tag_prefs = zeros(num_users, num_tags);
for user = 1:num_users
    for tag = 1:num_tags
        movies = tagged_movies{tag};
        
        numerator = 0;
        denominator = 0;
        
        for nn = 1:length(movies)
            movie = movies(nn);
            if user_movie_ratings(user, movie) ~= 0
                relevance = movie_tag_relevance(movie, tag);
                if relevance ~= 0
                    numerator = numerator + (relevance * double(user_movie_ratings(user, movie)));
                    denominator = denominator + relevance;
                else
                    'this should not happen'
                end
            end
        end
        
        if denominator ~= 0
            user_tag_prefs(user, tag) = numerator/denominator;
        end
    end
end

