% Iteratively prunes the movie-tag matrix (and updates the movie and tag indices to ensure
% that each non-pruned tag has a sequential index number; the index numbers of pruned 
% movies and tags are set to zero).

previous_size = size(movie_tag);
run('prune_movies.m');
run('prune_tags.m');
new_size = size(movie_tag);

while previous_size ~= new_size
    previous_size = size(movie_tag);
    run('prune_movies.m');
    run('prune_tags.m');
    new_size = size(movie_tag);
end