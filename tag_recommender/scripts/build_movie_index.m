% Movie IDs read from the files can be sparse, so this script reads the movie data file and maps each movie ID 
% to a sequential internal ID, so matrices can be indexed by movie ID without leaving empty rows, thus reducing
% the memory requirements of the algorithm.

if exist('movie_file', 'var') ~= 1
    movie_file = '../ml-10M100K/movies.dat';
end

fileID = fopen(movie_file,'r');
movies = textscan(fileID, '%u%q%s',  'Delimiter', {'::'}, 'headerLines', 0);
fclose(fileID);

movie_index = zeros(max(movies{1}), 1);
for index = 1:length(movies{1})
   movie_index(movies{1}(index)) = index; 
end