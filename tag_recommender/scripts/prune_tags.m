% Prune the movie-tag matrix to exclude tags with fewer than minimum_movies movies.

if exist('minimum_movies', 'var') ~= 1
    minimum_movies = 5;
end

delete_tags = [];
for tag = 1:size(movie_tag, 2)
    if nnz(movie_tag(:, tag)) < minimum_movies
        delete_tags = [delete_tags; tag];
    end
end

movie_tag(:, delete_tags) = [];