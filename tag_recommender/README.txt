TAG_RECOMMENDER: System to predict user ratings of movies based on information 
from user-generated tag applications to movies, and previous ratings of movies.

Functions are included to train a tag-based recommender system on supplied
training data, to predict the rating for a supplied (user, movie) pair,
and to test the system automatically on supplied training and testing data.

To run any of these functions, the scripts directory must be on your MATLAB path.

1) To calculate the matrices required to make rating predictions, use the
build_tagommender function, supplying the rating, tag and movie files in
the format specified by the MovieLens 10M dataset. Training hyperparameters
can be adjusted at the top of the function file.

2) To use the calculated matrices to make a prediction of a user's rating for a
given movie, use the function predict_user_rating, supplying the user ID,
movie ID and the outputs of the build_tagommender function. Prediction
hyperparameters can be adjusted at the top of the function file. If the system
is unable to make a tag-based prediction because the user or movie were pruned
during training, or because the user or movie were not encountered during training,
a zero rating is returned. Valid ratings are between 1 and 5.

3) To automatically test the system on supplied training and test data,
use the test_on_data function, supplying a training ratings file,
test ratings file, tag file and movie file in the format specified
by the MovieLens 10M dataset. 

If the system is asked to predict a rating for a (user, movie) pair
for which the system cannot produce a tag-based prediction because the
user or movie were pruned during training, the user's average rating
over the training data is used. If the user is not present at all in the
training data, the global average rating from the training data is used.

Results are returned in the form of a cell array with each row corresponding 
to a line in the test ratings file, with columns, from left to right:
User ID, Movie ID,correct rating from test data, predicted rating from system. 

Training and prediction hyperparameters can be adjusted from the build_tagommender
and predict_user_rating function files.